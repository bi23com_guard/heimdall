import logging
import json
import requests
import os
import time
from web3 import Web3, HTTPProvider
from web3.middleware import geth_poa_middleware
from thorchain.thorchain import ThorchainClient
from eth_keys import KeyAPI
from utils.common import Coin, get_rune_asset, Asset
from chains.aliases import aliases_celo, get_aliases, get_alias_address
from chains.chain import GenericChain

from web3.middleware import construct_sign_and_send_raw_middleware
from eth_account import Account

RUNE = get_rune_asset()


def calculate_gas(msg):
    return MockCelo.default_gas + Celo.gas_per_byte * len(msg)


class MockCelo:
    """
    An client implementation for a localnet/rinkebye/ropston Celo server
    """

    default_gas = 80000
    gas_price = 2
    seed = "SEED"
    stake = "ADD"
    passphrase = "bi23.com"

    private_keys = [
        "ef235aacf90d9f4aadd8c92e4b2562e1d9eb97f0df9ba3b508258739cb013db2",
        "289c2857d4598e37fb9647507e47a309d6133539bf21a8b9cb6df88fd5232032",
        "e810f1d7d6691b4a7a73476f3543bd87d601f9a53e7faf670eac2c5b517d83bf",
        "a96e62ed3955e65be32703f12d87b6b5cf26039ecfa948dc5107a495418e5330",
    ]

    def __init__(self, base_url, thor):

        self.url = base_url
        self.web3 = Web3(HTTPProvider(base_url))
        self.web3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.thor_chain = ThorchainClient(thor)
        self.accounts = self.web3.geth.personal.list_accounts()
        self.web3.eth.defaultAccount = self.accounts[0]

        for key in self.private_keys:
            address = self.web3.geth.personal.import_raw_key(key, self.passphrase)
            self.web3.geth.personal.unlock_account(
                address, self.passphrase, 0
            )
            account = Account.from_key(key)
            self.web3.middleware_onion.add(construct_sign_and_send_raw_middleware(account))

        path = os.path.split(os.path.realpath(__file__))
        contracts = os.path.join(path[0], '../data/celo_registry_contracts.json')
        with open(contracts) as json_file:
            contracts_data = json.load(json_file)
            golden_abi = contracts_data["GoldToken"]["ABI"]
            stable_abi = contracts_data["StableToken"]["ABI"]
            registry = contracts_data["Registry"]
            self.registry_contract = self.web3.eth.contract(address=registry["Address"], abi=registry["ABI"])
            self.gold_token_address = self.registry_contract.functions.getAddressForString("GoldToken").call()
            self.stable_token_address = self.registry_contract.functions.getAddressForString("StableToken").call()
            self.gold_token = self.web3.eth.contract(address=self.gold_token_address, abi=golden_abi)
            self.stable_token = self.web3.eth.contract(address=self.stable_token_address, abi=stable_abi)

        self.vault_contract = self.get_vault_contract()

        # transfer to the vault which pay the gas fee
        vault_address = self.thor_chain.get_vault_address("CELO")
        vault_address = self.web3.toChecksumAddress(vault_address)
        self.seed_gold_token(vault_address, 100000000000000000)

        self.seed_yggdrasil_vaults()

    @classmethod
    def get_address_from_pubkey(cls, pubkey):
        """
        Get Celo address for a specific hrp (human readable part)
        bech32 encoded from a public key(secp256k1).

        :param string pubkey: public key
        :returns: string 0x encoded address
        """
        eth_pubkey = KeyAPI.PublicKey.from_compressed_bytes(pubkey)
        return eth_pubkey.to_address()

    def seed_gold_token(self, address, amount):
        self.gold_token.functions.transfer(address, amount).transact({
            "gas": 200000
        })

    def set_vault_address(self, addr):
        """
        Set the vault eth address
        """
        aliases_celo["VAULT"] = addr

    def get_block_height(self):
        """
        Get the current block height of Ethereum localnet
        """
        block = self.web3.eth.getBlock("latest")
        return block["number"]

    def seed_yggdrasil_vaults(self):
        valuts = self.thor_chain.get_yggdrasil_vaults()
        for vault in valuts:
            addresses = vault["addresses"]
            for address in addresses:
                if address["chain"] == Celo.chain:
                    self.seed_gold_token(self.web3.toChecksumAddress(address["address"]), 100000000000000000)


    def get_vault_contract(self):
        vault = None
        asgard_vaults = self.thor_chain.get_asgard_vaults()
        for asgard_vault in asgard_vaults:
            if vault is not None:
                break
            routers = asgard_vault['routers']
            for router in routers:
                if router["chain"] == "CELO":
                    vault = router["router"]
                    break

        path = os.path.split(os.path.realpath(__file__))
        contract = os.path.join(path[0], "../data/celo_vault.json")

        with open(contract) as content:
            abi = json.load(content)
            vault = self.web3.eth.contract(vault, abi=abi)

        return vault

    def get_block_hash(self, block_height):
        """
        Get the block hash for a height
        """
        block = self.web3.eth.getBlock(block_height)
        return block["hash"].hex()

    def get_block_stats(self, block_height=None):
        """
        Get the block hash for a height
        """
        return {
            "avg_tx_size": 1,
            "avg_fee_rate": 1,
        }

    def set_block(self, block_height):
        """
        Set head for reorg
        """
        payload = json.dumps({"method": "debug_setHead", "params": [block_height]})
        headers = {"content-type": "application/json", "cache-control": "no-cache"}
        try:
            requests.request("POST", self.url, data=payload, headers=headers)
        except requests.exceptions.RequestException as e:
            logging.error(f"{e}")

    def ge_celo_balance(self, address):
        """
        Get Celo golden balance for an address
        """

        if address == "VAULT" or address == aliases_celo["VAULT"]:
            address = self.vault_contract.address

        return (
            self.web3.eth.getBalance(address)
        )

    def ge_cusd_balance(self, address):
        """
        Get Celo golden balance for an address
        """

        if address == "VAULT" or address == aliases_celo["VAULT"]:
            address = self.vault_contract.address

        return (
            self.stable_token.functions.balanceOf(address).call()
        )

    def wait_for_node(self):
        """
        Ethereum pow localnet node is started with directly mining 4 blocks
        to be able to start handling transactions.
        It can take a while depending on the machine specs so we retry.
        """
        current_height = self.get_block_height()
        while current_height < 2:
            current_height = self.get_block_height()

    def transfer(self, txn):
        """
        Make a transaction/transfer on localnet Celo
        """
        if not isinstance(txn.coins, list):
            txn.coins = [txn.coins]

        if txn.to_address in aliases_celo.keys():
            txn.to_address = get_alias_address(txn.chain, txn.to_address)

        if txn.from_address in aliases_celo.keys():
            txn.from_address = get_alias_address(txn.chain, txn.from_address)

        # update memo with actual address (over alias name)
        is_synth = txn.is_synth()
        for alias in get_aliases():
            chain = txn.chain
            asset = txn.get_asset_from_memo()
            if asset:
                chain = asset.get_chain()
            # we use RUNE BNB address to identify a cross chain liqudity provision
            if txn.memo.startswith(self.stake) or is_synth:
                chain = RUNE.get_chain()
            addr = get_alias_address(chain, alias)
            txn.memo = txn.memo.replace(alias, addr)

        self.web3.eth.defaultAccount = self.accounts[0]
        for account in self.web3.eth.accounts:
            if account.lower() == txn.from_address.lower():
                self.web3.eth.defaultAccount = account
        spent_gas = 0

        asset_address = self.stable_token_address
        erc20 = self.stable_token
        symbol = 'CUSD'
        if txn.coins[0].asset.get_symbol() == Celo.chain:
            asset_address = self.gold_token_address
            erc20 = self.gold_token
            symbol = 'CELO'

        if txn.memo == self.seed:

            tx_hash = erc20.functions.transfer(txn.to_address, txn.coins[0].amount).transact({
                "gas": 200000
            })
            receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
            spent_gas = receipt.gasUsed

        else:
            tx_hash = (
                erc20.functions.approve(
                    Web3.toChecksumAddress(self.vault_contract.address), txn.coins[0].amount
                )
                .transact({
                    "gas": 200000
                })
            )

            receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)
            spent_gas = receipt.gasUsed
            tx_hash = self.vault_contract.functions.deposit(
                Web3.toChecksumAddress(txn.to_address),
                Web3.toChecksumAddress(asset_address),
                txn.coins[0].amount,
                txn.memo.encode("utf-8"),
                int(time.time()) + 86400
            ).transact({
                "gas": 200000
            })

            receipt = self.web3.eth.waitForTransactionReceipt(tx_hash)

            txn.id = receipt.transactionHash.hex()[2:].upper()

        txn.gas = [
            Coin(
                f"CELO.{symbol}",
                receipt.gasUsed + spent_gas,
            )
        ]


class Celo(GenericChain):
    """
    A local simple implementation of Ethereum chain
    """

    name = "Celo"
    gas_per_byte = 68
    chain = "CELO"
    coin = Asset("CELO.CELO")
    withdrawals = {}
    swaps = {}

    @classmethod
    def _calculate_gas(cls, pool, txn):
        """
        Calculate gas according to RUNE thorchain fee
        """
        gas = 39540
        if txn.gas is not None and txn.gas[0].asset.is_celo():
            gas = txn.gas[0].amount
        if txn.memo == "WITHDRAW:CELO.CELO:1000":
            gas = 39849
        elif txn.memo == "SWAP:CELO.CELO":
            gas = 39837
        elif txn.memo == "SWAP:CELO.CUSD":
            gas = 52996
        elif txn.memo == "WITHDRAW:CELO.CUSD":
            gas = 44647
        elif txn.memo == "WITHDRAW:CELO.CELO":
            gas = 39861
        return Coin(cls.coin, gas * 3)
